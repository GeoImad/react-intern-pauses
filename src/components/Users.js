import React , {useState, useEffect} from 'react'

export const Users = () => {

    const [test, setTest] = useState(0);

    useEffect (() => {
        console.log("test changed")
    }
    ,[test])
    
    return (
        <div>
            <button onClick={() => setTest(test+1)}>Test</button>
            {test}

        </div>
    )
}

export default Users;
