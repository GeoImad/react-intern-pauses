import logo from './logo.svg';
import './App.css';
import react , {useState} from 'react';
import Pauses from './components/Pauses';
import Pause from './components/Pause';
import Users from './components/Users';

function App() {

  const theUsers = [
      {
      id : 1,
      pseudo : 'Imad',
      password :'123',
      type : 'user',
      team : [{
        pseudo : 'Abdelilah',
        },
        {
        pseudo : 'Saad',
        },
        {
        pseudo : 'Yahya',
        }]
      },
      {
      id : 2,
      pseudo : 'Abdelilah',
      password :'123',
      type : 'user',
      team : [{
        pseudo : 'Imad',
      },
      {
        pseudo : 'Saad',
      },
      {
        pseudo : 'Yahya',
      }]
    },
    {
      id : 3,
      pseudo : 'Saad',
      password :'123',
      type : 'user',
      team : [{
        pseudo : 'Imad',
      },
      {
        pseudo : 'Abdelilah',
      },
      {
        pseudo : 'Yahya',
      }  
      ]
    },
    {
      id : 4,
      pseudo : 'Yahya',
      password :'123',
      type : 'user',
      team : [{
        pseudo : 'Imad',
      },
      {
        pseudo : 'Abdelilah',
      },
      {
        pseudo : 'Saad',
      }]
    },

  ];

  const [pause, setpause] = useState([
    {
      id : 1,
      dateDemande : 10.5,
      dateDebut : 10.5,
      etat : 'true',
      idUser :2,
    },
    {
      id : 2,
      dateDemande : 10.5,
      dateDebut : 10.5,
      etat : 'true',
      idUser :3,
    },
    {
      id : 3,
      dateDemande : 10.5,
      dateDebut : 10.75,
      etat : 'false',
      idUser :1,
    },
    {
      id : 4,
      dateDemande : 10.66,
      dateDebut : 10.75,
      etat : 'false',
      idUser :4,
    },
    
  ]);
const deleteUser = (id) => {
  setpause(
    pause.filter((p)=>
      p.id != id 
    )
  )
}

const passUser = (id) => {
  setpause(
    [...(pause.filter((p)=>
      p.id != id 
    )) , pause.find((p)=>
    p.id == id 
  )]
  
  )
}

const passNext = (id) => {
  let back = pause.find((p)=> p.id == id);
  let next = pause.find((p)=> p.id == id+1);
  let before = pause.filter((p)=> p.id < id);
  let after = pause.filter((p)=> p.id > id+1);
 

  setpause(
    [...before, next , back,...after]
    //pause.splice(id-1,2,next,back)
    
  )
  
}

  return (
    <div className="App">
      <Pauses theUsers={theUsers} pause={pause} deleteUser={deleteUser} passUser={passUser} passNext={passNext}></Pauses>
      <Users></Users>
    </div>
  );
}

export default App;
